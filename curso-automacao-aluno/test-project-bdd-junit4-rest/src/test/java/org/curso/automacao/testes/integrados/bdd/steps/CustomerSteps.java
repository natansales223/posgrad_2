package org.curso.automacao.testes.integrados.bdd.steps;

import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.helpers.entities.Customer;
import org.curso.automacao.testes.integrados.actions.CustomerActions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CustomerSteps extends BaseSteps {

	private Customer customer;

	private RequestSpecification requestSpecification;
	private Response response;

	@Dado("que existam clientes cadastrados na base de dados")
	public void que_existam_clientes_cadastrados_na_base_de_dados() throws Exception {
		customer = CustomerActions.getCustomerValid();
	}
	@Quando("eu preparar a requisição para listar todos os clientes via API")
	public void eu_preparar_a_requisição_para_listar_todos_os_clientes_via_api() throws Exception {
		logInfo("Preparing the request specification.");
		requestSpecification = CustomerActions.getAllCustomerRequestSpecification();
	}
	@Quando("realizar a chamada para listar todos os clientes via API")
	public void realizar_a_chamada_para_listar_todos_os_clientes_via_api() throws Exception {
		logInfo("Executing the request to authentication api [" + getUrl("services.customer.url")
				+ CustomerActions.CUSTOMER_ALL_ENDPOINT + "]");
		response = CustomerActions.requestAllCustomer(requestSpecification);
	}
	@Então("a lista de clientes deverá ser retornada com sucesso via API")
	public void a_lista_de_clientes_deverá_ser_retornada_com_sucesso_via_api() {
		logInfo("Validate if customer was succesfully created.");
		CustomerActions.validateAllCustomerResponse(response);
	}

	@Dado("que existam paises cadastrados na base de dados")
	public void que_existam_paises_cadastrados_na_base_de_dados() {
		//mock
	}
	@Quando("eu preparar a requisição para listar todos os países disponíveis via API")
	public void eu_preparar_a_requisição_para_listar_todos_os_países_disponíveis_via_api() throws Exception {
		logInfo("Preparing the request specification.");
		requestSpecification = CustomerActions.getAllCountriesRequestSpecification();
	}
	@Quando("realizar a chamada para listar todos os países disponíveis via API")
	public void realizar_a_chamada_para_listar_todos_os_países_disponíveis_via_api() throws Exception {
		logInfo("Executing the request to authentication api [" + getUrl("services.customer.url")
				+ CustomerActions.COUNTRIES_ALL_ENDPOINT + "]");
		response = CustomerActions.requestAllCountries(requestSpecification);
	}
	@Então("a lista de países deverá ser retornada com sucesso via API")
	public void a_lista_de_países_deverá_ser_retornada_com_sucesso_via_api() {
		logInfo("Validate if customer was succesfully created.");
		CustomerActions.validateAllCountriesResponse(response);
	}

	@Quando("eu preparar a requisição para buscar um cliente por ID via API")
	public void eu_preparar_a_requisição_para_buscar_um_cliente_por_id_via_api() throws Exception {
		logInfo("Preparing the request specification.");
		requestSpecification = CustomerActions.getIdCustomerRequestSpecification(customer);
	}
	@Quando("realizar a chamada para buscar um cliente por ID via API")
	public void realizar_a_chamada_para_buscar_um_cliente_por_id_via_api() throws Exception {
		logInfo("Executing the request to authentication api [" + getUrl("services.customer.url")
				+ CustomerActions.CUSTOMER_SAVE_ENDPOINT + "]");
		response = CustomerActions.requestIdCustomer(requestSpecification, customer.getId());
	}
	@Então("o cliente com ID especificado deverá ser retornado com sucesso via API")
	public void o_cliente_com_id_especificado_deverá_ser_retornado_com_sucesso_via_api() {
		logInfo("Validate if customer was succesfully created.");
		CustomerActions.validateIdCustomerResponse(customer, response);
	}

	@Dado("que eu tenha as informações válidas para cadastro de um cliente via API")
	public void que_eu_tenha_as_informações_válidas_para_cadastro_de_um_cliente_via_api() {

		logInfo("Prepare data for new customer.");
		customer = CustomerActions.getNewCustomer();
	}

	@Quando("eu preparar a requisição de cadastro do cliente via API")
	public void eu_preparar_a_requisição_de_cadastro_do_cliente_via_api() throws Exception {
		logInfo("Preparing the request specification.");
		requestSpecification = CustomerActions.getNewCustomerRequestSpecification(customer);
	}

	@Quando("realizar a chamada de cadastro do cliente via API")
	public void realizar_a_chamada_de_cadastro_do_cliente_via_api() throws Exception {
		logInfo("Executing the request to authentication api [" + getUrl("services.customer.url")
				+ CustomerActions.CUSTOMER_SAVE_ENDPOINT + "]");
		response = CustomerActions.requestNewCustomer(requestSpecification);
	}

	@Então("o cliente deverá ser cadastrado com sucesso via API")
	public void o_cliente_deverá_ser_cadastrado_com_sucesso_via_api() {
		logInfo("Validate if customer was succesfully created.");
		CustomerActions.validateNewCustomerResponse(customer, response);
	}

	@Dado("que exista um cliente com ID válido na base de dados")
	public void que_exista_um_cliente_com_id_válido_na_base_de_dados() throws Exception {
		customer = CustomerActions.getCustomerValid();
	}
	@Dado("eu tenha informações válidas para atualizar o cliente via API")
	public void eu_tenha_informações_válidas_para_atualizar_o_cliente_via_api() {
		Customer customerUpdate = CustomerActions.getUpdateCustomer(customer);
	}
	@Quando("eu preparar a requisição para atualizar o cliente via API")
	public void eu_preparar_a_requisição_para_atualizar_o_cliente_via_api() throws Exception {
		requestSpecification = CustomerActions.getUpdateCustomerRequestSpecification(customer);
	}
	@Quando("realizar a chamada para atualizar o cliente via API")
	public void realizar_a_chamada_para_atualizar_o_cliente_via_api() {
		response = CustomerActions.requestUpdateCustomer(requestSpecification);
	}
	@Então("o cliente deverá ser atualizado com sucesso via API")
	public void o_cliente_deverá_ser_atualizado_com_sucesso_via_api() {
		CustomerActions.validateUpdateCustomerResponse(customer, response);
	}

	@Quando("eu preparar a requisição para excluir um cliente por ID via API")
	public void eu_preparar_a_requisição_para_excluir_um_cliente_por_id_via_api() throws Exception {
		requestSpecification = CustomerActions.getDeleteCustomerRequestSpecification(customer);
	}
	@Quando("realizar a chamada para excluir um cliente por ID via API")
	public void realizar_a_chamada_para_excluir_um_cliente_por_id_via_api() {
		response = CustomerActions.requestDeleteCustomer(requestSpecification, customer.getId());
	}
	@Então("o cliente com ID especificado deverá ser excluído com sucesso via API")
	public void o_cliente_com_id_especificado_deverá_ser_excluído_com_sucesso_via_api() {
		CustomerActions.validateDeleteCustomerResponse(customer, response);
	}
}
