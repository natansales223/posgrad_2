# language: pt

@env.dev @clientes
Funcionalidade: Gerenciamento de Clientes
  Como administrador da aplicação, desejo poder gerenciar os clientes por meio de uma API.

  Contexto: Autenticação bem-sucedida do usuário administrador
    Dado que tenho credenciais válidas "admin@automacao.org.br" e "password01" para autenticação na aplicação via API
    Quando eu preparar a requisição para a API
    E efetuar a chamada dessa requisição na API
    Então a autenticação deve ser realizada com sucesso.

  @testes-integracao
  Cenário: Listar todos os clientes via API
    Dado que existam clientes registrados na base de dados
    Quando eu preparar a requisição para listar todos os clientes via API
    E efetuar a chamada para listar todos os clientes via API
    Então a lista de clientes deve ser retornada com sucesso via API

  @testes-integracao
  Cenário: Listar todos os países disponíveis via API
    Dado que existam países registrados na base de dados
    Quando eu preparar a requisição para listar todos os países disponíveis via API
    E efetuar a chamada para listar todos os países disponíveis via API
    Então a lista de países deve ser retornada com sucesso via API

  @testes-integracao
  Cenário: Buscar um cliente por ID via API
    Dado que existe um cliente com um ID válido na base de dados
    Quando eu preparar a requisição para buscar um cliente por ID via API
    E efetuar a chamada para buscar um cliente por ID via API
    Então o cliente com o ID especificado deve ser retornado com sucesso via API

  @testes-integracao
  Cenário: Cadastro de cliente via API
    Dado que tenho informações válidas para cadastrar um cliente via API
    Quando eu preparar a requisição de cadastro do cliente via API
    E efetuar a chamada de cadastro do cliente via API
    Então o cliente deve ser cadastrado com sucesso via API

  @testes-integracao
  Cenário: Atualizar um cliente via API
    Dado que existe um cliente com um ID válido na base de dados
    E tenho informações válidas para atualizar o cliente via API
    Quando eu preparar a requisição para atualizar o cliente via API
    E efetuar a chamada para atualizar o cliente via API
    Então o cliente deve ser atualizado com sucesso via API

  @testes-integracao
  Cenário: Excluir um cliente por ID via API
    Dado que existe um cliente com um ID válido na base de dados
    Quando eu preparar a requisição para excluir um cliente por ID via API
    E efetuar a chamada para excluir um cliente por ID via API
    Então o cliente com o ID especificado deve ser excluído com sucesso via API
