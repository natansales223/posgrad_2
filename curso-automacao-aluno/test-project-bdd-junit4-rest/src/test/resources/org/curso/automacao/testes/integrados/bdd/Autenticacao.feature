# language: pt

@env.dev @auth
Funcionalidade: Autenticação de Usuário
  Eu, como usuário, desejo poder realizar autenticação na aplicação por meio da API de autenticação.

  @testes-integracao
  Esquema do Cenário: Autenticação de Usuários na Aplicação
    Dado que tenho credenciais válidas "<usuário>" e "<senha>" para autenticação na aplicação via API
    Quando eu preparar a requisição para a API
    E efetuar a chamada dessa requisição na API
    Então a autenticação deve ser realizada com sucesso.

    Exemplos: 
      | usuário                | senha        |
      | admin@automacao.org.br | password01   |
