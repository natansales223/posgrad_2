# language: pt

@env.dev @usuarios
Funcionalidade: Gerenciamento de Usuários
  Eu, como gestor da aplicação, desejo gerenciar os usuários por meio de uma API.

  Contexto: Autenticação bem-sucedida do usuário administrador
    Dado que tenho credenciais válidas "admin@automacao.org.br" e "password01" para autenticação na aplicação via API
    Quando eu preparar a requisição para a API
    E efetuar a chamada dessa requisição na API
    Então a autenticação deve ser realizada com sucesso.

  @testes-integracao
  Cenário: Listar todos os usuários da aplicação via API
    Dado que tenho um token válido para a minha chamada de API
    Quando eu preparar a requisição para obter a lista de usuários na API
    E efetuar a chamada para obter a lista
    Então deverei ver a lista de usuários preenchida
