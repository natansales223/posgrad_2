pacote org.curso.automacao.testes.sistemas.actions;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;
import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.sistemas.pages.PaginaUsuario;
import org.curso.automacao.testes.sistemas.pages.FactoryPaginaPrincipal;

import static org.junit.Assert.assertTrue;

public class AcoesUsuario {

    public static String nomeUsuario = StringUtils.EMPTY;
    public static String idUsuario = StringUtils.EMPTY;

    public static PaginaUsuario paginaUsuario() {
        return FactoryPaginaPrincipal.getPagina(PaginaUsuario.class);
    }

    public static void validarPaginaUsuario() {
        paginaUsuario().validarPagina();

        // Capturar a evidência da tela
        BaseSteps.capturarScreenshot();
    }

    public static void preencherFormulario() {

        Faker faker = Faker.instance();

        nomeUsuario = faker.name().fullName();

        String senha = faker.internet().password();

        paginaUsuario().preencherFormulario(nomeUsuario,
                faker.internet().emailAddress(),
                senha,
                senha,
                "ROLE_ADMIN, ROLE_USER",
                true);

        // Capturar a evidência da tela
        BaseSteps.capturarScreenshot();
    }

    public static void preencherFormularioComNomeAtualizado() {

        Faker faker = Faker.instance();

        nomeUsuario = faker.name().fullName();

        paginaUsuario().preencherFormulario(nomeUsuario,
                faker.internet().emailAddress(),
                StringUtils.EMPTY,
                StringUtils.EMPTY,
                StringUtils.EMPTY,
                true);

        // Capturar a evidência da tela
        BaseSteps.capturarScreenshot();
    }

    public static void clicarEmEnviar() {

        paginaUsuario().clicarEmEnviar();
        // Capturar a evidência da tela
        BaseSteps.capturarScreenshot();

        idUsuario = paginaUsuario().getTxtIdUsuario().getText();
    }

    public static void clicarEmUsuarios() {

        paginaUsuario().clicarEmUsuarios();
        // Capturar a evidência da tela
        BaseSteps.capturarScreenshot();
    }

    public static void validarUsuarioSalvoComSucesso() {

        assertTrue("Validação se o usuário está na tela", paginaUsuario().getLblUsuarioSalvoComSucesso().isDisplayed());
    }
}
