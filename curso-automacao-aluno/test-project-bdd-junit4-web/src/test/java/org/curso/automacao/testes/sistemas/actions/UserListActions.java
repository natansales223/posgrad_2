package org.curso.automacao.testes.sistemas.actions;

import static org.junit.Assert.assertTrue;

import org.curso.automacao.testes.BaseSteps;
import org.curso.automacao.testes.sistemas.pages.UserListPage;
import org.curso.automacao.testes.sistemas.pages.MasterPageFactory;

public class UserListActions {

    public static UserListPage userListPage() {
        return MasterPageFactory.getPage(UserListPage.class);
    }

    public static void clickOnCreateNewUser() {
        userListPage().getBtnCreateUser().click();
    }

    public static void validateUserListPage() {
        userListPage().validatePage();

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }

    public static void search(String value) {

        userListPage().getTxtSearch().sendKeys(value);

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }

    public static void checkValueInResultList(String value) {

        assertTrue("Validate if user was found in the result list.", userListPage().findValueInTable(value).isDisplayed());
    }

    public static void clickOnUpdateButton() {
        userListPage().findFirstButtonUpdateInTable().click();
    }

    public static void clickOnDeleteButton() {
        userListPage().findFirstButtonDeleteInTable().click();
    }

    public static void acceptAlertDeleteConfirmation() {
        userListPage().acceptAlertDeleteConfirmation();

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();
    }

    public static void validateNoMatchingRecordsFound() {

        userListPage().findNoMatchingRecordsFound();

        // Colher a evidência da tela
        BaseSteps.takeScreenshot();

    }

}
