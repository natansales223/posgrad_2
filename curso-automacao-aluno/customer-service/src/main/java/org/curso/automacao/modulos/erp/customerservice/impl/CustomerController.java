package org.curso.automacao.modulos.erp.customerservice.impl;

import java.util.List;

import org.curso.automacao.modulos.erp.customerservice.common.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.github.javafaker.Faker;

@RestController
@RequestMapping("/api/v1/clientes")
public class CustomerController extends BaseController<CustomerService, JpaRepository<Customer, Long>, Customer> {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/todos")
    public ResponseEntity<List<Customer>> obterTodosClientes() { // Renomeei o método para ficar mais descritivo
        return super.findAll();
    }

    @GetMapping("/paises/todos")
    public ResponseEntity<List<String>> obterTodosPaises() { // Renomeei o método para ficar mais descritivo
        return new ResponseEntity<>(customerService.obterTodosPaises(), HttpStatus.OK);
    }

    @GetMapping("/encontrar-por/id/{id}")
    public ResponseEntity<Customer> encontrarClientePorId(@PathVariable("id") long id) { // Renomeei o método para ficar mais descritivo
        return super.findById(id);
    }

    @PostMapping("/criar") // Use POST para criar um novo cliente
    public ResponseEntity<Customer> criarCliente(@RequestBody @Validated Customer entidade) { // Renomeei o método para ficar mais descritivo
        return super.salvar(entidade);
    }

    @PutMapping("/atualizar/{id}") // Use PUT para atualizar um cliente específico
    public ResponseEntity<Customer> atualizarCliente(@PathVariable("id") long id, @RequestBody @Validated Customer entidade) { // Renomeei o método para ficar mais descritivo
        return super.atualizar(entidade);
    }

    @DeleteMapping("/excluir/{id}") // Use DELETE para excluir um cliente específico
    public ResponseEntity<Customer> excluirClientePorId(@PathVariable("id") long id) { // Renomeei o método para ficar mais descritivo
        return super.excluirPorId(id);
    }
}
