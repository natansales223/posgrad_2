package org.curso.automacao.modulos.erp.customerservice.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.curso.automacao.modulos.erp.customerservice.common.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "customers")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Customer extends BaseEntity {
    
    @Column(name = "salary", precision = 2, nullable = false)
    private double salary; // Use double para representar valores decimais com precisão
    
    @Column(name = "address")
    @NotBlank(message = "O endereço é obrigatório")
    private String address;
    
    @Column(name = "email", unique = true) // Adicionei a restrição de email único
    @NotBlank(message = "O email é obrigatório")
    @Email(message = "Email inválido")
    private String email;
    
    @Column(name = "phone_number")
    @NotBlank(message = "O número de telefone é obrigatório")
    private String phoneNumber;
    
    @Column(name = "company")
    private String company;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "state")
    private String state;
    
    @Column(name = "country")
    private String country;
    
    @Column(name = "zipcode")
    @Size(max = 10, message = "O código postal deve ter no máximo 10 caracteres")
    private String zipcode;
}
